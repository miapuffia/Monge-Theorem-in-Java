To run:

Download both files.
Run the Monge's Theorem.bat file with Main.java in the same directory.
Prey you have a current enough version of Java installed with the proper classpath setup.
Profit (hopefully).

The program will make one additional file (Main.class) upon using the bat file. Other than that, no change will be made to your computer. The program needs no installation and is portable (alongside Java, that is).

If anyone is interested in how the program works I can comment the code for easy reading. Any improvements are welcome too.